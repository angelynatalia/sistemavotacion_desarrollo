package Entidad;

import java.time.LocalDateTime;
import java.util.Scanner;
import ufps.util.varios.ServicioEmail;

public class Notificacion {

    private String nombreDpto;

    private String nombreMunicipio;

    private LocalDateTime fechaVotacion;

    private Persona persona;

    public Notificacion() {
    }

    public Notificacion(String nombreDpto, String nombreMunicipio, LocalDateTime fechaVotacion, Persona persona) {
        this.nombreDpto = nombreDpto;
        this.nombreMunicipio = nombreMunicipio;
        this.fechaVotacion = fechaVotacion;
        this.persona = persona;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public LocalDateTime getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(LocalDateTime fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String crearNotificacion(CentroVotacion c, Mesa m) {
        String msg = asignarMensaje() + " " + this.nombreMunicipio + " en:" + c + "\n" + m;
//       enviarEmail(msg);
        return msg;
    }

    private void enviarEmail(String msg){
        String emailUsuarioEmisor="ejemplo.email.ufps@gmail.com";
        String clave="nfrbdxklxggkgoko";
        String emailReceptor=this.persona.getEmail();
        ServicioEmail email=new ServicioEmail(emailUsuarioEmisor, clave);
        email.enviarEmail(emailReceptor, "Elecciones", msg);
    }
    
    private String asignarMensaje() {
        if (this.persona.isEsJurado()) {
            return "Usted ha sido designado jurado de votación para las elecciones en ";
        }
        if (this.persona.isEsSufragante()) {
            return "Su puesto de votación para laas elecciones está ubicado en ";
        }

        return "Usted se ha quedado sin centro de votación.";
    }

    @Override
    public String toString() {
        return "Notificacion{" + persona.toString() + ": " + asignarMensaje() + nombreMunicipio + ", Fecha Votacion=" + fechaVotacion + " }";
    }

}
