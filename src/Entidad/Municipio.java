package Entidad;

import ufps.util.colecciones_seed.ListaCD;

public class Municipio {

    private int id_municipio;

    private String nombre;

    private ListaCD<CentroVotacion> centros = new ListaCD();

    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaCD<CentroVotacion> getCentros() {
        return centros;
    }

    public void setCentros(ListaCD<CentroVotacion> centros) {
        this.centros = centros;
    }

    public CentroVotacion getCentroAleatorio() {
        int i = (int) (Math.random() * ((this.centros.getTamanio() - 1) - 0 + 1) + 0);
        return this.centros.get(i);
    }

    public String getMesasAnuladas() {
        String msg = "";
        for (CentroVotacion centro : centros) {
            msg += centro.getMesasAnuladas();
        }
        return msg;
    }

    private String getListadoCentros() {
        String msg = "";
        for (CentroVotacion c : this.centros) {
            msg += c.toString();
        }
        return msg;
    }

    @Override
    public String toString() {
        return "\nMunicipio{" + "id_municipio=" + id_municipio + ", nombre=" + nombre + getListadoCentros() + '}';
    }

}
