package Entidad;

import ufps.util.colecciones_seed.ListaCD;

public class Departamento {

    private int id_dpto;

    private String nombreDpto;

    private ListaCD<Municipio> municipios = new ListaCD();

    public Departamento() {
    }

    public Departamento(int id_dpto, String nombreDpto) {
        this.id_dpto = id_dpto;
        this.nombreDpto = nombreDpto;
    }

    public Municipio buscarMun(int id_mun) {
        for (Municipio mun : this.municipios) {
            if (mun.getId_municipio() == id_mun) {
                return mun;
            }
        }
        return null;
    }

    public String getMesasAnuladas() {
        String msg = "";
        for (Municipio municipio : municipios) {
            msg += municipio.getMesasAnuladas();
        }
        return msg;
    }

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }

    @Override
    public String toString() {
        return "Departamento{" + "id_dpto=" + id_dpto + ", nombreDpto=" + nombreDpto
                + this.getListadoMunicipios()
                + "}\n\n";
    }

    public String getListadoMunicipios() {
        String msg = "";
        for (Municipio dato : this.getMunicipios()) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

}
