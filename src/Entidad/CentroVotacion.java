package Entidad;

import ufps.util.colecciones_seed.Pila;

public class CentroVotacion {

    private int id_centro;

    private String nombreCentro;

    private String direccion;

    private Pila<Mesa> mesas = new Pila();

    private int cantidadSufragantes;

    public CentroVotacion() {
    }

    public CentroVotacion(int id_centro, String nombreCentro, String direccion, int cantidad) {
        this.id_centro = id_centro;
        this.nombreCentro = nombreCentro;
        this.direccion = direccion;
        this.cantidadSufragantes = cantidad;
    }

    public void crearMesas(int cant) {
        if (cant <= 0) {
            return;
        }

        while (cant > 0) {
            this.mesas.apilar(new Mesa(cant));
            cant--;
        }
    }

    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCantidadSufragantes() {
        return cantidadSufragantes;
    }

    public void setCantidadSufragantes(int cantidadSufragantes) {
        this.cantidadSufragantes = cantidadSufragantes;
    }

    public Pila<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(Pila<Mesa> mesas) {
        this.mesas = mesas;
    }

    public String getMesasAnuladas() {
        if (mesas.esVacia()) {
            return "";
        }
        String msg = "";
        Pila<Mesa> p = new Pila();

        while (!mesas.esVacia()) {
            if (!mesas.getTope().tieneJurados()) {
                p.apilar(mesas.getTope());
                msg += "\n"+this.getNombreCentro()+", "+this.getId_centro()+"\n"+mesas.desapilar();
            } else {
                p.apilar(mesas.desapilar());
            }
        }
        cargarPila(p);
        return msg;
    }
    
    public Mesa getMesaAleatorio() {
        if (this.mesas.esVacia()) {
            return null;
        }

        int i = (int) (Math.random() * (this.mesas.getTamanio() - 1 + 1) + 1);
        
        Pila<Mesa> p = new Pila();

        while (!this.mesas.esVacia()) {
            if (i == 1) {
                cargarPila(p);
                return this.mesas.desapilar();
            }
            p.apilar(this.mesas.desapilar());
            i--;
        }
        
        return null;
    }

    private void cargarPila(Pila<Mesa> p) {
        while (!p.esVacia()) {
            this.mesas.apilar(p.desapilar());
        }
    }

    public String getListadoMesas() {
        if (this.mesas.esVacia()) {
            return "";
        }

        String msg = "";
        Pila<Mesa> p = new Pila();

        while (!this.mesas.esVacia()) {
            p.apilar(new Mesa(this.mesas.getTope().getId_mesa()));
            msg += "\n" + this.mesas.desapilar().toString();
        }
        cargarPila(p);
        return msg;
    }

    @Override
    public String toString() {
        return "\nCentroVotacion{" + "id_centro=" + id_centro + ", nombreCentro=" + nombreCentro + ", direccion=" + direccion + ", cantidadSufragantes=" + cantidadSufragantes
                + "\nMesas: " + this.getListadoMesas() + '}';
    }
}
