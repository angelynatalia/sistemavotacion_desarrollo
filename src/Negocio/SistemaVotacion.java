package Negocio;

import Entidad.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class SistemaVotacion {

    private ListaCD<Departamento> dptos = new ListaCD();
    private ListaCD<Persona> personas = new ListaCD();
    private Cola<Notificacion> notificaciones = new Cola();

    public SistemaVotacion(String urlDptos, String urlMunicipios, String urlPer, String urlCentros) {
        crearDptos(urlDptos);
        crearMunicipios(urlMunicipios);
        crearPersonas(urlPer);
        crearCentros(urlCentros);
    }

    private void crearCentros(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {

            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            //id_centro;nombreCentro;direccion;id_municipio;cantMesas;maximoSufragantes
            int id_centro = Integer.parseInt(datos2[0]);
            String nombreCentro = datos2[1];
            String direccion = datos2[2];
            int id_municipio = Integer.parseInt(datos2[3]);
            int cantMesas = Integer.parseInt(datos2[4]);
            int maxSufr = Integer.parseInt(datos2[5]);
            Municipio mun = buscarMun(id_municipio);

            if (mun != null) {
                CentroVotacion c = new CentroVotacion(id_centro, nombreCentro, direccion, maxSufr);
                c.crearMesas(cantMesas);

                mun.getCentros().insertarAlFinal(c);
            }
        }
    }

    private Municipio buscarMun(int id_mun) {

        for (Departamento dep : this.dptos) {
            Municipio mun = dep.buscarMun(id_mun);
            if (mun != null) {
                return mun;
            }
        }
        return null;
    }

    private void crearPersonas(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            long cedula = Long.parseLong(datos2[0]);
            String nombre = datos2[1];
            LocalDateTime fechaNacimiento = crearFecha(datos2[2]);
            int idMuni = Integer.parseInt(datos2[3]);
            String email = datos2[4];
            //Crear personas
            Persona nueva = new Persona(cedula, nombre, fechaNacimiento, idMuni, email);

            this.personas.insertarAlFinal(nueva);
        }
    }

    private LocalDateTime crearFecha(String fecha) {
        String datos3[] = fecha.split("-");
        int agno = Integer.parseInt(datos3[0]);
        int mes = Integer.parseInt(datos3[1]);
        int dia = Integer.parseInt(datos3[2]);

        return LocalDateTime.of(agno, mes, dia, 0, 0, 0, 0);

    }

    private void crearMunicipios(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio    
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            int id_muni = Integer.parseInt(datos2[1]);
            String nombreMun = datos2[2];
            Municipio nuevo = new Municipio(id_muni, nombreMun);

            Departamento dpto = buscarDpto(id_dpto);
            if (dpto != null) {
                dpto.getMunicipios().insertarAlFinal(nuevo);
            }
        }
    }

    private Departamento buscarDpto(int id) {
        for (Departamento dato : this.getDptos()) {
            if (dato.getId_dpto() == id) {
                return dato;
            }
        }
        return null;
    }

    private void crearDptos(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            Departamento nuevo = new Departamento(id_dpto, datos2[1]);
            this.dptos.insertarAlFinal(nuevo);
        }
    }

    private Persona getPersonaAleatorio() {
        int i = (int) (Math.random() * ((this.personas.getTamanio() - 1) - 0 + 1) + 0);
        return this.personas.get(i);
    }

    
    
    private boolean todosSonSufragentes()
    {
    for(Persona dato:personas)
            if(!(dato.isEsJurado() || dato.isEsSufragante()))
                return true;
    return false;
    }
    
    public void asignarJurados() {
        //ESTA LÓGICA ES A NIVEL MUY MUY MUY GENERAL
        
        //ojo ud tiene que iterar hasta que los estado de las personas sean todos true
        //while(todosSOnSufrantes){
        for (Persona persona : personas) {
            Persona dato = getPersonaAleatorio();
            if (!dato.isEsJurado()) {
                int idInscripcion = dato.getId_Municipio_inscripcion();
                Municipio muni = this.buscarMun(idInscripcion);
                CentroVotacion centro = muni.getCentroAleatorio();

                if (centro != null) {
                    Mesa mesa = centro.getMesaAleatorio();
                    if (mesa != null) {
                        if (mesa.asignarJurado(dato)) {
                            //crear notificacion
                            Notificacion n = new Notificacion("", muni.getNombre(), crearFecha("2019-10-27"), dato);
                            n.crearNotificacion(centro, mesa);
                            this.notificaciones.enColar(n);
                        }
                    }
                }
            }
        }
    }

    public void asignarSufrangantes() {

        for (Persona p : personas) {

            Persona persona = getPersonaAleatorio();
            if (!persona.isEsJurado()) {

                int idInscripcion = persona.getId_Municipio_inscripcion();
                Municipio mun = this.buscarMun(idInscripcion);
                CentroVotacion c = mun.getCentroAleatorio();

                if (c != null) {
                    Mesa mesa = c.getMesaAleatorio();
                    if (mesa != null) {
                        if (mesa.asignarSufragantes(persona)) {
                            //crear notificacion
                            Notificacion n = new Notificacion("", mun.getNombre(), crearFecha("2019-10-27"), persona);
                            n.crearNotificacion(c, mesa);
                            this.notificaciones.enColar(n);
                        }
                    }
                }
            }
        }
    }

    public String proceso() {
        String msg = "";

        asignarJurados();
        asignarSufrangantes();

        msg += "\nCola de notificaciones: \n" + getListadoNotificaciones();
        msg += "\nMesas Anuladas: \n" + getMesasAnuladas();
        msg += "\nPersonas sin centros: \n" + getPersonasSinCentros();

        return msg;
    }

    private String getListadoNotificaciones() {
        String msg = "";
        Cola<Notificacion> c = new Cola();

        while (!this.notificaciones.esVacia()) {
            msg += this.notificaciones.getInfoInicio() + "\n";
            c.enColar(this.notificaciones.deColar());
        }
        llenarCola(c);
        return msg;
    }

    private void llenarCola(Cola<Notificacion> c) {
        while (!c.esVacia()) {
            this.notificaciones.enColar(c.deColar());
        }
    }

    private String getPersonasSinCentros() {
        String msg = "";

        for (Persona persona : personas) {
            if (!persona.isEsJurado() && !persona.isEsSufragante()) {
                msg += persona;
            }
        }
        return msg;
    }

    private String getMesasAnuladas() {
        String msg = "";
        for (Departamento dpto : dptos) {
            msg += dpto.getMesasAnuladas();
        }
        return msg;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public String getListadoDpto() {
        String msg = "";
        for (Departamento datos : this.dptos) {
            msg += datos.toString() + "\n";
        }
        return msg;
    }

    public String getListadoPersonas() {
        String msg = "";
        for (Persona datos : this.personas) {
            msg += datos.toString() + "\n";
        }
        return msg;

    }

}
